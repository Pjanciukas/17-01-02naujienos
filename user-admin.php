<?php

session_start();
if ($_SESSION['isAdmin'] == 1) 
{
	include "config.php";

	$user = $_SESSION['username'];
} else {
	header('Location: index.php');
}

$sql = "SELECT * FROM users ORDER BY id ASC";

$results = $db->query($sql);

if($results->num_rows > 0) {
	$i = 0;
		
	while ($row = $results->fetch_assoc()) {
		$data[$i] = $row;
		$i++;
	}
			
} else {
	$data['no_entries'] = 'Jus neturite vartotoju';
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienu portalas</title>
	<link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	
	<script type="text/javascript" src="js/script.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>

<body>

    <!-- Navigation -->
	<?php include "header.php" ?>

	<div id="mySidenav" class="sidenav">
		<a href="admin.php">Administravimas</a>
		<a href="entry.php">Naujas irasas</a>
		<a href="user-entries.php">Mano naujienos</a>
		<a href="comm-admin.php">Komentarai</a>
		<a href="user-admin.php">Vartotojai</a>
	</div>
    <!-- Page Content -->
    <div class="container" style="margin-left: 300px;">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-center">Sveiki, <?=$user;?> !
                    <!-- <small>Secondary Text</small> -->
                </h1>
            </div>
        </div>
        <!-- /.row -->
		<div>
						<?php 
				foreach ($data as $user) {
				?>
					<li class="list-group-item" > <p>Vardas: <b><?=$user['username']?></b> ID: <b><?=$user['id']?></b> Status: <b><?php 
						if($user['status'] == 0) 
						{
							echo "Active";
						} else if($user['status'] == 1) 
						{
							echo "Admin";
						} else {
							echo "Inactive";
						} ?></b> <br> 
						<div class="btn-group" role="group" aria-label="..." aria-expanded="false">
							<form action="edit-user.php" method="post"> 
								<input type="hidden" name="userSelector" value="<?=$user['id']?>">
								<input type="hidden" name="userStatus" value="0">
								<button type="submit"  class="btn btn-default">User</button>
							</form>
							<form action="edit-user.php" method="post"> 
								<input type="hidden" name="userSelector" value="<?=$user['id']?>">
								<input type="hidden" name="userStatus" value="1">
								<button type="submit"  class="btn btn-default">Admin</button>
							</form>
							<form action="edit-user.php" method="post"> 
								<input type="hidden" name="userSelector" value="<?=$user['id']?>">
								<input type="hidden" name="userStatus" value="2">
								<button type="submit"  class="btn btn-default">Trinti</button>
							</form>
						</div>
					</li>
					
				<?php 
				}
			?>
		</div>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
