<?php 
session_start();
if ($_SESSION['isAdmin'] == 1) 
{

	include "config.php";


	$sql = "SELECT * FROM categories";
	$results = $db->query($sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienu portalas</title>
	<script src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=lsj5waqhmicg3nqrwoctmkj8v3dtycenrzp1i3yhxywfotz7"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
    <link href="css/style.css" rel="stylesheet">
	<!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	
	
	<script type="text/javascript" src="js/script.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>

<body>

    <!-- Navigation -->
	<?php include "header.php"?>
	
	<div id="mySidenav" class="sidenav">
		<a href="admin.php">Administravimas</a>
		<a href="entry.php">Naujas irasas</a>
		<a href="user-entries.php">Mano naujienos</a>
		<a href="comm-admin.php">Komentarai</a>
		<a href="user-admin.php">Vartotojai</a>
	</div>
    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-center">Naujienų įkėlimas
                    <!-- <small>Secondary Text</small> -->
                </h3>
            </div>
        </div>
        <!-- /.row -->
		<div class="row">
			<form class="form-horizontal" action="db-entry.php" method="post">
			  <div class="form-group">
					<label class="control-label col-sm-4" for="username">Kategorija:</label>
					<div class="col-sm-6">
					  <select name="id" class="form-control">
						<?php 
							$i = 0;
			
							while ($row = $results->fetch_assoc()) {
								$data[$i] = $row;
								$i++;
							}
							
							foreach($data as $entry) {
								$category = $entry['category'];
								$id = $entry['id']; ?>
								
								<option value="<?=$id?>"><?=$category?></option>
						<?php	}
						
						?>
					  </select>
					</div>
			  </div>
			  <div class="form-group">
				<label class="control-label col-sm-4" for="username">Antraštė:</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="title" name="title" placeholder="antraštė" required>
				</div>
			  </div>
			   <div class="form-group">
				<label class="control-label col-sm-4" for="username">Nuotraukos url:</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="url" name="url" placeholder="url" required>
				</div>
			  </div>
			  <div class="form-group">
				<label class="control-label col-sm-4" for="pwd">Turinys:</label>
				<div class="col-sm-6"> 
				  <textarea name="entry"></textarea>
				</div>
			  </div>
			  <div class="form-group"> 
				<div class="col-sm-offset-4 col-sm-6">
				  <button type="submit" class="btn btn-default">Įkelti</button>
				</div>
			  </div>
		</div>
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery --> 
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
<?php
} else {
	header('Location: index.php');
}
?>
