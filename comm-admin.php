<?php
session_start();
if ($_SESSION['isAdmin'] == 1) 
{
	include "config.php";

	$userId = $_SESSION['user_id'];

	$sql = "SELECT * FROM entries WHERE user_id = '{$userId}' ORDER BY timestamp DESC";

	$results = $db->query($sql);

	if($results->num_rows > 0) {
		$i = 0;
		
		while ($row = $results->fetch_assoc()) {
			$data[$i] = $row;
			$i++;
		}
			
	} else {
		
		$data['no_entries'] = 'Jus neturite irasu';
	}
} else {
	 header('Location: index.php');
}
 
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	
    <title>Naujienu portalas</title>
	<script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
    <!-- Bootstrap Core CSS -->
	<link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	
    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>

<body>

	<?php include "header.php"?>
	
	
	
    <!-- Page Content -->
    <div class="container" style="margin-left:275px;">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-center">Mano naujienos
                    <!-- <small>Secondary Text</small> -->
                </h3>
            </div>
        </div>
		
		<div id="mySidenav" class="sidenav">
			<a href="admin.php">Administravimas</a>
			<a href="entry.php">Naujas irasas</a>
			<a href="user-entries.php">Mano naujienos</a>
			<a href="comm-admin.php">Komentarai</a>
			<a href="user-admin.php">Vartotojai</a>
		</div>	
		
        <!-- /.row -->
		<div class="row">
		
		<?php if(isset($data['no_entries'])) {
	
				echo "<p>" . $data['no_entries'] . "</p>";
				} else {
	
					foreach($data as $entry) {
			  ?>
			<div  class="panel-group" aria-expanded="false" id="accordion" role="tablist" aria-multiselectable="true" >
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading<?=$entry['id']?>">
						<h4 class="panel-title">
							<a role="button"  data-toggle="collapse" aria-expanded="false" data-parent="#accordion" href="#collapse<?=$entry['id']?>"  aria-controls="collapse<?=$entry['id']?>">
							<?=$entry['title']?>
							</a>
						</h4>
					</div>
					<div id="collapse<?=$entry['id']?>" aria-expanded="false" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="heading<?=$entry['id']?>">
						<div class="list-group" >
							<?php
								$sqlComments = "SELECT * FROM comments WHERE entry_id =" . $entry['id'] . " ORDER BY timestamp DESC";

								$results = $db->query($sqlComments);

								if($results->num_rows > 0) {
									$i = 0;
										
									while ($row = $results->fetch_assoc()) {
										$comments[$i] = $row;
										$i++;
									}
											
								} else {
										
									$comments['no_entries'] = 'Komentaru nera';
								}

								if(isset($comments['no_entries'])) 
								{
									echo "<br><p>" . $comments['no_entries'] . "</p>";
								} else 
								{
									foreach ($comments as $comment) 
									{
										$userComment = $comment['user_id'];
										$sqlComments = "SELECT * FROM users WHERE id = '{$userComment}'";
										$results = $db->query($sqlComments);
										if($results->num_rows == 1)
										{
											$author = $results->fetch_assoc();
										} else 
										{
											$author['username'] = "Anonimas";
										}
										
									?>
										<li class="list-group-item" > <p>Autorius: <b><?=$author['username']?></b> Laikas: <b><?=$comment['timestamp']?></b> <br> <?=$comment['comment']?></p> 
											<div class="btn-group" role="group" aria-label="..." aria-expanded="false">
												<form action="delete-comment.php" method="post"> 
													<input type="hidden" name="commentDelete" value="<?=$comment['id']?>"></input>
													<button type="submit"  class="btn btn-default">Trinti</button>
												</form>
												<form action="open-comment.php" method="get"> 
													<input type="hidden" name="commentEdit" value="<?=$comment['id']?>">
													<button type="submit"  class="btn btn-default">Pakeisti</button>
												</form>
											</div>
										</li>
									<?php

									}
								}
							?>
						</div>
					</div>
				</div>		
			</div>
						
		
				<?php }
				}
				?>
		</div>	  
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery --> 
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

