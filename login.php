<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienu portalas</title>
	<link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
			#registration-error {
				display: none;
			}

	</style>
</head>

<body >

    <!-- Navigation -->
    <?php include "header.php"?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-center">Prisijungti
                    <!-- <small>Secondary Text</small> -->
                </h1>
            </div>
        </div>
        <!-- /.row -->
		<div class="row">
			<form id="login-form" class="form-horizontal">
			  <div class="form-group">
				<label class="control-label col-sm-4" for="username">Vartotojo vardas:</label>
				<div class="col-sm-6">
				  <input id="username" type="text" class="form-control" id="email" name="username" placeholder="Iveskite vartotoja" required>
				</div>
			  </div>
			  <div class="form-group">
				<label class="control-label col-sm-4" for="pwd">Slaptažodis:</label>
				<div class="col-sm-6"> 
				  <input id="password" type="password" class="form-control" id="pwd" name="password" placeholder="Iveskite slaptazodi" required>
				</div>
			  </div>
			  <!--<div class="form-group"> 
				<div class="col-sm-offset-4 col-sm-6">
				  <div class="checkbox">
					<label><input type="checkbox" id="adminCheck" name="checkbox" > Admin</label>
				  </div>
				</div>
			  </div>-->
			  <div class="form-group"> 
				<div class="col-sm-offset-4 col-sm-6">
					<button type="submit" class="btn btn-default">Prisijungti</button>
				</div>
			  </div>
			  <p id="registration-error" class="alert alert-danger"><strong>Tokio vartotojo nėra!</strong></p>
		</div>
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<script>
		$(document).ready(function() {
					$('#login-form').submit(function(event){
					
						event.preventDefault();
						
						var pass1 = $('#password').val();
						var username = $('#username').val();
						
							
							$.ajax({
								url: 'db-login.php',
								type: 'post',
								data: {'username': username, 'password' : pass1},
								success: function(data){
									if(data == 'Sis vartotojas neaktyvus'){
										$('#registration-error').show();
									} else if(data == 'admin') {
										window.location.assign('index.php');
									} else if(data == 'user') {
										window.location.assign('index.php');
									} else {
										$('#registration-error').show();
									}
								}
								
							});
							
					});
					$('#username').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
					$('#password').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
					$('#password2').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
				});
	
	</script>

</body>

</html>
