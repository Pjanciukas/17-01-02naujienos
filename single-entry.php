<?php
include "config.php";
session_start();

$entryId = $_GET['entryId'];
$sql = "SELECT * FROM entries WHERE id = '{$entryId}'";

if(isset($_SESSION['username']))
{
	$userId = $_SESSION['user_id'];
}
$results = $db->query($sql);

if ($results->num_rows > 0) {
	
	$i = 0;
	
	while($row = $results->fetch_assoc()) {
		
		$data[$i] = $row;
	}
	
} else {
		$data['no_entries'] = "tokio įrašo nėra";
}?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienu portalas</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>

<body>

    <!-- Navigation -->
	<?php include "header.php"?>
    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
       
        <!-- /.row -->
		<div class="row">

		<?php if(isset($data['no_entries'])) {
	
		echo "<p>" . $data['no_entries'] . "</p>";
		} else {
			
			foreach($data as $entry) {
				?>
				<div class="rcorner">
					<h1 class="entry-header"><?=$entry['title'];?></h1>
					<img class="img-responsive" src="<?=$entry['img_url']?>" alt="" width="400px" height="400px">
					<p><?=$entry['entry'];?></p>
					<p> <?=$entry['timestamp'];?> </p>
				</div>
				
				<?php
				include "comments.php"
				?>
				
		<?php }
		}
		?>

			  
		</div>
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery --> 
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

