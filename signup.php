<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user = $_SESSION['username'];
} else {
	$user = "";
}

// if(isset($_SESSION['signup_message'])) {
	// $message = $_SESSION['signup_message'];
	// $message = $message;
	
// } else {
	// $message = "";
// }

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienų portalas</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
			#wrong-pass-error, 
			#registration-error {
				display: none;
			}
			#registration-success {
				color: green;
				display: none;
			}
	
	</style>

</head>

<body>

    <!-- Navigation -->
	<?php include "header.php"?>
	
    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-center">Vartotojo registracija
                    <!-- <small>Secondary Text</small> -->
                </h3>
            </div>
        </div>
        <!-- /.row -->
		<div class="row">
			<form id="registration-form" class="form-horizontal">
			  <div class="form-group">
				<label class="control-label col-sm-4" for="username">Vartotojo vardas:</label>
				<div class="col-sm-6">
				  <input id="username" type="text" class="form-control" id="username" name="username" placeholder="Įveskite vardą" required>
				</div>
			  </div>
			  <div class="form-group">
				<label class="control-label col-sm-4" for="pwd">Slaptažodis:</label>
				<div class="col-sm-6"> 
				  <input id="password" type="password" class="form-control" id="pwd" name="password" placeholder="Įveskite slaptažodį" required>
				</div>
			  </div>
			  <div class="form-group">
				<label class="control-label col-sm-4" for="pwd">Pakartoti slaptažodį:</label>
				<div class="col-sm-6"> 
				  <input id="password2" type="password" class="form-control" id="pwd" name="password" placeholder="Įveskite slaptažodį" required>
				</div>
			  </div>
			  <div class="form-group"> 
				<div class="col-sm-offset-4 col-sm-6">
				  <button type="submit" class="btn btn-default">Registruoti</button>
				</div>
			  </div>
			  
			  	<p id="wrong-pass-error" class="alert alert-danger"><strong>Slaptažodžiai nevienodi!</strong></p>
				<p id="registration-error" class="alert alert-danger"><strong>Vartotojas jau užregistruotas!</strong></p>
				<p id="registration-success" class="alert alert-success"><strong>Vartotojas sėkmingai užregistruotas!</strong></p>
			  
		</div>
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function() {
					$('#registration-form').submit(function(event){
					
						event.preventDefault();
						
						var pass1 = $('#password').val();
						var pass2 = $('#password2').val();
						var username = $('#username').val();
						
						if(pass1 == pass2) {
							
							$.ajax({
								url: 'db-signup.php',
								type: 'post',
								data: {'username': username, 'password' : pass1},
								success: function(data){
									if(data == 'registered'){
										$('#registration-success').show();
									} else {
										$('#registration-error').show();
									}
								}
								
							});
							
							
							
							
							
							
						} else {
							$('#password').css('border','1px solid red');
							$('#password2').css('border','1px solid red');
							$('#wrong-pass-error').show();
						}
						
					
					});
					$('#username').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
					$('#password').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
					$('#password2').click(function(){
						$('#wrong-pass-error').hide();
						$('#registration-error').hide();
					});
					
				});
	
	</script>

</body>

</html>
