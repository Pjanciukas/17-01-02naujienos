-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2017 at 06:38 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naujienos`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Mokslas'),
(2, 'IT naujienos');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `entry_id`, `user_id`, `comment`, `timestamp`, `status`) VALUES
(1, 3, 1, 'labas as krabas ir megstu surio desreles', '2017-02-13 16:41:11', 1),
(2, 3, 2, 'eik jau tu, koks dar krabas? grabas ne krabas tu xDDDDDD', '2017-02-13 16:41:35', 1),
(3, 3, NULL, 'abu jus lopai. salin valdzia, tegyvuoja spageti monstras!', '2017-02-13 16:42:16', 1),
(4, 3, NULL, 'Mano komentaras', '2017-02-13 17:28:47', 1),
(5, 4, NULL, 'fgdfgdgfd', '2017-02-13 18:16:18', 1),
(6, 4, NULL, 'gjgjhg', '2017-02-13 18:16:22', 1),
(7, 4, NULL, 'iyiyi', '2017-02-13 18:16:31', 1),
(8, 3, 1, 'wrwerwer', '2017-02-13 18:27:00', 1),
(9, 17, 1, 'Komentuoju', '2017-02-14 12:40:21', 1),
(10, 16, 1, 'ir dar komentaras', '2017-02-14 12:41:23', 1),
(11, 19, 2, 'Mano komentaras', '2017-02-14 13:12:56', 1),
(12, 19, NULL, 'dar komentaras', '2017-02-14 13:13:22', 1),
(13, 21, 1, 'dfdfdfdfd', '2017-02-14 13:40:51', 1),
(14, 23, 1, 'Komentuooju', '2017-02-14 14:46:14', 1),
(15, 26, NULL, 'As komentuoju', '2017-02-15 12:22:29', 1),
(16, 29, NULL, 'Komentuoju', '2017-02-15 17:16:59', 1),
(17, 31, NULL, 'Komentuojam. Labas, as krabas!', '2017-02-15 17:28:15', 1),
(18, 30, NULL, 'Labas as kitas krabas!', '2017-02-15 17:30:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `entry` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int(11) NOT NULL,
  `img_url` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `user_id`, `title`, `entry`, `timestamp`, `category_id`, `img_url`, `status`) VALUES
(3, 1, 'ASD asd asd asd asd asd ', '<p>fsfafdsfd fsdfsdfasfgfgasgsa</p>', '2017-02-12 23:40:21', 0, '', 0),
(4, 1, 'dadsa', '<p>asdasdad</p>', '2017-02-12 23:57:05', 0, '', 0),
(5, 1, 'test', '<p>test</p>', '2017-02-13 00:12:47', 0, '', 0),
(6, 1, 'er', '<p>er</p>', '2017-02-13 00:25:42', 0, '', 0),
(7, 1, 'bbbb', '<p>bbbb</p>', '2017-02-13 00:54:03', 0, '', 0),
(8, 1, 'uyuyu', '<p>yuyuyu</p>', '2017-02-13 00:56:24', 0, '', 0),
(9, 1, 'rrr', '<p>rrr</p>', '2017-02-13 01:13:35', 0, '', 0),
(11, 1, 'ggggggg', '<p>gggggg</p>', '2017-02-13 17:06:52', 0, '', 0),
(12, 1, 'Naujas irasas', '<p>Naujas irasas</p>', '2017-02-13 19:48:09', 0, '', 0),
(13, 1, 'Ar praeina?', '<p>Ar praeina?</p>', '2017-02-13 19:50:42', 0, '', 0),
(14, 1, 'rtrtrt', '<p>rtrtrt</p>', '2017-02-13 19:52:26', 0, '', 0),
(15, 1, 'Id dar naujas irasas', '<p>Id dar naujas irasas.&nbsp;Id dar naujas irasas.&nbsp;Id dar naujas irasas.</p>', '2017-02-13 19:59:09', 0, '', 0),
(16, 1, 'Mokslo naujienos', '<p>Mokslo naujienos.&nbsp;Mokslo naujienos.&nbsp;Mokslo naujienos.&nbsp;Mokslo naujienos.</p>', '2017-02-14 13:49:09', 1, '', 0),
(17, 1, 'IT naujiena', '<p>IT naujiena.&nbsp;IT naujiena.&nbsp;IT naujiena.&nbsp;IT naujiena.</p>', '2017-02-14 13:49:50', 2, '', 0),
(18, 1, 'Ir vel mokslas.', '<p>Ir vel mokslas.&nbsp;Ir vel mokslas.&nbsp;Ir vel mokslas.&nbsp;Ir vel mokslas.&nbsp;Ir vel mokslas.</p>', '2017-02-14 14:43:57', 1, '', 0),
(19, 1, 'Paveikslas', '<p>&lt;img class="img-responsive"&nbsp;src="img/i.jpg"&gt;</p>', '2017-02-14 14:55:59', 1, '', 0),
(20, 1, 'Foto', '<p>&amp;lt;img class="img-responsive"&amp;nbsp;src="img/i.jpg"&amp;gt;</p>', '2017-02-14 14:58:24', 2, '', 0),
(21, 1, 'Ne visai mokslas', '<p>Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas. Ne visai mokslas.</p>', '2017-02-14 15:27:59', 1, '', 0),
(22, 1, '', '', '2017-02-14 16:13:12', 0, '', 0),
(23, 1, 'nu gal nar IT biski', '<p>nu gal nar IT biski.&nbsp;nu gal nar IT biski.&nbsp;nu gal nar IT biski.&nbsp;nu gal nar IT biski.</p>', '2017-02-14 16:44:08', 2, '', 0),
(24, 1, 'IT naujiena su nuotrauka', '<p>IT naujiena su nuotrauka. IT naujiena su nuotrauka. IT naujiena su nuotrauka. IT naujiena su nuotrauka.</p>', '2017-02-14 20:57:40', 2, 'img/3.jpg', 0),
(25, 1, 'Nu dabar tikrai su paveiksleliu', '<p>Micronas. Micronas. Micronas. Micronas. Micronas. Micronas. Micronas. Micronas.</p>', '2017-02-14 21:09:08', 2, 'img/micron.jpg', 0),
(26, 1, 'Mokslo naujienos', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis, purus vel vestibulum bibendum, dolor dui auctor est, quis finibus magna purus non tellus. Etiam euismod lacus turpis, non pulvinar tortor tristique sed. Pellentesque laoreet convallis ante. Donec commodo nulla quis ante pharetra egestas. Donec viverra ornare sem, sed cursus lacus. Maecenas venenatis eleifend sapien, eu interdum libero porttitor nec. Pellentesque ultrices dapibus porttitor. Sed elementum vehicula leo feugiat rhoncus.</p>', '2017-02-15 13:30:52', 1, 'img/micron.jpg', 0),
(27, 1, 'Wifi technologijos', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis, purus vel vestibulum bibendum, dolor dui auctor est, quis finibus magna purus non tellus. Etiam euismod lacus turpis, non pulvinar tortor tristique sed. Pellentesque laoreet convallis ante. Donec commodo nulla quis ante pharetra egestas. Donec viverra ornare sem, sed cursus lacus. Maecenas venenatis eleifend sapien, eu interdum libero porttitor nec. Pellentesque ultrices dapibus porttitor. Sed elementum vehicula leo feugiat rhoncus.</p>', '2017-02-15 15:27:55', 2, 'img/wifi.jpg', 0),
(28, 1, 'Lorem Ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pharetra orci ac lacus ullamcorper semper. Praesent bibendum est eu commodo vulputate. Donec finibus tellus orci, eu egestas orci posuere ut. Suspendisse at dolor ut felis placerat congue. Curabitur at purus vitae tellus eleifend semper. Integer non odio sollicitudin, vehicula nibh non, luctus odio. Phasellus dapibus urna laoreet elit condimentum viverra. Donec mollis nunc vel tortor egestas suscipit. Curabitur congue a est vitae sagittis. Suspendisse potenti. Curabitur ornare metus orci, a facilisis mi ultrices ac. In euismod, neque id vehicula pellentesque, libero nisl molestie diam, eu sodales nisi est vitae lacus.</p>\r\n<p>Nam viverra pretium tempus. Sed eu libero vitae nisl bibendum dictum id vitae mi. Vivamus placerat lobortis justo at ultricies. Morbi consectetur imperdiet sem, quis porttitor tortor tincidunt et. Vestibulum ut venenatis lorem, quis rhoncus neque. Integer sapien arcu, egestas eget pharetra sed, facilisis id ante. Phasellus nisi felis, commodo aliquam nibh et, mollis ullamcorper neque. Proin massa nunc, mattis ac justo sed, accumsan faucibus est.</p>\r\n<p>Fusce molestie, urna imperdiet mollis volutpat, elit quam cursus risus, sit amet gravida urna dolor vitae nunc. Praesent ullamcorper, libero eget accumsan tempor, turpis est tincidunt mi, in commodo leo urna non metus. Phasellus efficitur ultrices ex. Praesent non eros augue. Mauris gravida varius massa tincidunt sagittis. Sed at nisi lorem. Nullam porta scelerisque lorem vitae viverra. Vivamus dignissim enim sit amet urna lacinia rhoncus. Nam nec orci ut dolor posuere semper vitae in velit.</p>', '2017-02-15 18:28:24', 1, 'img/wifi.jpg', 0),
(29, 1, 'â€žiPhoneâ€œ ir â€žAndroidâ€œ stovyklÅ³ atstovÅ³ tarpusavio bendravimas turÄ—tÅ³ bÅ«ti panaÅ¡us Ä¯ Å¡uns ir katÄ—s. ', '<p>Tyrimo rezultatai ai&scaron;kiai sako: jeigu turÄ—damas &bdquo;Android&ldquo; telefonÄ… ateisite Ä¯ pirmÄ… romantinÄ¯ pasimatymÄ… su &bdquo;iPhone&ldquo; savininku ar savininke, tikimybÄ— susilaukti teigiamo vertinimo sumaÅ¾Ä—s net 21 kartÄ…, nei ki&scaron;enÄ—je turint taip pat &bdquo;iPhone&ldquo;, ra&scaron;o &bdquo;The Verge&ldquo;. Tuo tarpu &bdquo;Android&ldquo; telefono savininkai Å¾mogÅ³, pasimatyme pasirodÅ¾iusÄ¯ su oponuojanÄios stovyklos i&scaron;maniuoju telefonu, teigiamai Ä¯vertins &bdquo;tik&ldquo; 15 kartÅ³ reÄiau, nei turintÄ¯ &bdquo;Android&ldquo; telefonÄ…. KÄ… tai pasako apie &bdquo;iPhone&ldquo; savininkus? Sunku pasakyti, bet galima kelti prielaidÄ…, kad &bdquo;Apple&ldquo; gerbÄ—jai yra i&scaron;didesni, &bdquo;pasikÄ—lÄ™ elito atstovai&ldquo;, kurie ki&scaron;enines technologijas vertina tarsi socialinÄ—s padÄ—ties rodiklÄ¯, o savo 800 doleriÅ³ ar eurÅ³ kainuojantÄ¯ aparatÄ… mano esant ai&scaron;kiu socialinÄ—s vir&scaron;enybÄ—s prie&scaron; kitÄ… Å¾mogÅ³ Å¾enklu. Kita vertus, gal padÄ—tis visai nÄ—ra tokia bloga ir neigiamÄ… Ä¯spÅ«dÄ¯ apie kitos veislÄ—s telefonus &bdquo;iPhone&ldquo; savininkai susidaro dÄ—l to, kad nesupranta, kaip Å¾inutÄ—s gali bÅ«ti Å¾alios, o ne mÄ—lynos. Bet dar blogiau uÅ¾ &bdquo;Android&ldquo; ir &bdquo;Apple&ldquo; turÄ—tojÅ³ nesuderinamumÄ… yra moterÅ³ nesuderinamumas su skilusiu vyri&scaron;kio telefono ekranu. To paties tyrimo metu i&scaron;ai&scaron;kÄ—jo, kad moterys 86 proc. daÅ¾niau neigiamai Ä¯vertins savo pasimatymo partnerÄ¯ vien dÄ—l to, kad jo telefono ekranas sudauÅ¾ytas.<br /><br />Skaitykite daugiau: http://www.15min.lt/mokslasit/straipsnis/technologijos/pirmas-iphone-ir-android-turetoju-pasimatymas-veikiausiai-taps-ir-paskutiniu-646-755762</p>', '2017-02-15 18:48:06', 1, 'img/phone.jpg', 1),
(30, 1, 'Turime pratintis prie minties, kai greta mÅ«sÅ³ â€žapsigyvensâ€œ elektroniniai asmenys â€“ robotai, kurie ne tik padÄ—s Å¾monÄ—ms atlikti daugelÄ¯ darbÅ³, pakeis juos pavojingose situacijose ar stebins dirbtiniu intelektu, bet ir sukels Å¾alos.', '<p>TodÄ—l Europos Komisija (EK) raginama paruo&scaron;ti taisykles, kurios, be kita ko, numatytÅ³ jiems civilinÄ™ atsakomybÄ™ &ndash; lyg avarijÄ… sukÄ—lusiems vairuotojams, o gamintojus Ä¯pareigotÅ³ sukurti specialÅ³ mygtukÄ…, kuris leistÅ³ &bdquo;nuÅ¾udyti&ldquo; netinkamai dirbantÄ¯ robotÄ…. &bdquo;DÄ—l Ä¯spÅ«dingos pastarojo de&scaron;imtmeÄio technologinÄ—s paÅ¾angos &scaron;iandienos robotai gali ne tik atlikti veiksmus, kuriuos paprastai ir i&scaron;skirtinai atlikdavo Å¾mogus, bet ir tam tikrÅ³ autonominiÅ³ ir kognityviniÅ³ savybiÅ³ (pvz., gebÄ—jimo mokytis i&scaron; patirties ir beveik savaranki&scaron;kai priimti sprendimus) plÄ—tojimas vis labiau paverÄia juos subjektais, sÄ…veikaujanÄiais su aplinka bei galinÄiais jÄ… atitinkamai keisti. DÄ—l tokios padÄ—ties teisinÄ—, i&scaron; Å¾alingÅ³ roboto veiksmÅ³ kylanti atsakomybÄ— tampa esminiu klausimu&ldquo;, &ndash; teigiama atitinkamoje rezoliucijoje, kuriÄ… ketvirtadienÄ¯ rengiasi priimti Europos Parlamentas (EP).<br /><br />Skaitykite daugiau: http://www.15min.lt/mokslasit/straipsnis/technologijos/robotams-teks-atsakyti-uz-padaryta-zala-lyg-avarija-sukelusiems-vairuotojams-646-755670</p>', '2017-02-15 19:23:14', 1, 'img/robot.jpg', 1),
(31, 1, 'â€žFacebookâ€œ taps triukÅ¡mingesniu ', '<p>Oficialiame tinklara&scaron;tyje &bdquo;Facebook&ldquo; &scaron;iÄ… tinklo veikimo permainÄ… Ä¯vardina kaip &bdquo;gyvenimo kokybÄ—s pagerinimÄ…&ldquo;: &bdquo;Å½monÄ—s tiesiog tikisi, kad jiems susidomÄ—jus garso Ä¯ra&scaron;u bus girdimas ir jo garsas, tad automatinis garso Ä¯jungimas suteikia sklandesnÄ™ patirtÄ¯. Be to, taip uÅ¾tikrinama, kad naudotojÅ³ ausis pasieks reklamÅ³ garsas. Laimei, garsas nebus Ä¯jungiamas telefonuose, kurie tuo metu veikia tylos reÅ¾imu, be to, naudotojai gali savaranki&scaron;kai i&scaron;jungti automatinÄ¯ garso paleidimÄ… nustatymuose radÄ™ eilutÄ™ &bdquo;Videos in News Feed Start With Sound&ldquo;. NaujovÄ— visus socialinio tinklo naudotojus turÄ—tÅ³ pasiekti iki &scaron;iÅ³ metÅ³ pabaigos, ra&scaron;o engadget.com.<br /><br />Skaitykite daugiau: http://www.15min.lt/mokslasit/straipsnis/technologijos/facebook-taps-triuksmingesniu-646-755618</p>', '2017-02-15 19:25:18', 2, 'img/facebook.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', 'admin', 1),
(2, 'user', 'user', 0),
(3, 'admin2', 'admin2', 0),
(4, 'admin3', 'admin3', 0),
(5, 'admin4', 'admin4', 0),
(6, 'admin5', 'admin5', 0),
(7, 'user1', 'user1', 0),
(8, 'user10', 'user10', 0),
(9, 'user2', 'user2', 0),
(10, 'testas', '123', 0),
(11, 'jurgisTest', 'jurgis', 2),
(12, 'www', 'www', 2),
(13, 'rrr', 'rrr', 0),
(14, 'ooo', 'ooo', 0),
(15, 'vcs', 'vcs', 0),
(16, 'yyy', 'yyy', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `entries`
--
ALTER TABLE `entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
