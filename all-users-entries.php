<?php
session_start();
	include "config.php";

	//$userId = $_SESSION['user_id'];
	$categoryId = $_GET['id'];
	
	if(isset($_GET['byDate'])) {
		$sort = $_GET['byDate'];
		if($sort == 1) {
		$sql = "SELECT * FROM entries WHERE category_id = '{$categoryId}' AND status='1' ORDER BY timestamp ASC";
		
		
		} else {
			$sql = "SELECT * FROM entries WHERE category_id = '{$categoryId}' AND status='1' ORDER BY timestamp DESC";
		}
	} else {
		$sql = "SELECT * FROM entries WHERE category_id = '{$categoryId}' AND status='1' ORDER BY timestamp DESC";
	}
	
	
	//$sql = "SELECT * FROM entries WHERE category_id = '{$categoryId}' ORDER BY timestamp DESC";

	$results = $db->query($sql);

	if($results->num_rows > 0) {
		$i = 0;
		
		while ($row = $results->fetch_assoc()) {
			$data[$i] = $row;
			$i++;
		}
			
	} else {
		
		$data['no_entries'] = 'Jūs neturite įrašų';
	}
 
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naujienu portalas</title>
	<script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
    <!-- Bootstrap Core CSS -->
    <link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>

<body>

	<?php include "header.php"?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
				<?php if($categoryId == 1) { ?>
                <h3 class="page-header text-center">Mokslas
                    <!-- <small>Secondary Text</small> -->
                </h3>
				<?php } else { ?>
				 <h3 class="page-header text-center">IT naujienos
                    <!-- <small>Secondary Text</small> -->
                </h3>
				<?php } ?>
            </div>
        </div>
        <!-- /.row -->
		<div class="list-group row">
		<div class="btn-group">
		  <a href="filters.php?byDate=1&id=<?=$categoryId?>" type="button" class="btn btn-info">Seniausi viršuje</a>
		  <a href="filters.php?byDate=2&id=<?=$categoryId?>" type="button" class="btn btn-info">Naujausi viršuje</a>
		</div>

		</div>
		
		<?php if(isset($data['no_entries'])) {
	
				echo "<p>" . $data['no_entries'] . "</p>";
				} else {
	
				foreach($data as $entry) {
					
				$out = strlen($entry['entry']) > 500 ? substr($entry['entry'],0,500)."..." : $entry['entry'];
			  ?>
				<!--<div class="list-group">
					<a href="single-entry.php?entryId=<?=$entry['id']?>" class="list-group-item list-group-item-info">
					<p><?=$entry['title']?><span class="text_red"> (10)</span> </p> 
					<p><?=$entry['timestamp'];?> </p>
					</a>
					
				</div>-->
				<div class="row">
					<div class="col-md-4 portfolio-item">
					<a href="single-entry.php?entryId=<?=$entry['id']?>">
							<img class="img-responsive" src="<?=$entry['img_url']?>" alt="" width="700px" height="400px">
						</a>
					</div>
					<div class="col-md-8 portfolio-item">
						
						<h3>
							<a href="single-entry.php?entryId=<?=$entry['id']?>"><?=$entry['title']?></a>
						</h3>
						<p><?=$out?></p>
						<p><?=$entry['timestamp']?></p>
					</div>
				</div>
				<?php }
				}
				?>
			  
		
		
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery --> 
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

