    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Tech News</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="all-users-entries.php?id=1">Mokslas</a>
                    </li>
                    <li>
                        <a href="all-users-entries.php?id=2">IT </a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
					<?php if(isset($_SESSION['username'])) { ?>
					
						<?php if($_SESSION['username'] === 'admin') { ?>
						<li>
							<a href="admin.php"></span><?=$_SESSION['username']?></a>
						</li>
						<?php } else {?>
						<li>
							<a href="user.php"></span><?=$_SESSION['username']?></a>
						</li>
						<?php } ?>
                    <li>
                        <a href="logout.php"><span class="glyphicon glyphicon-user"></span> Atsijungti</a>
                    </li>
					
					<?php } else { ?>
						<li>
							<a href="login.php"><span class="glyphicon glyphicon-user"></span> Prisijungti</a>
						</li>
					<?php } ?>
                    <li>
                        <a href="signup.php"><span class="glyphicon glyphicon-log-in"></span> Registruotis</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
